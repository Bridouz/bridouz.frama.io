+++
+++

Te voila arrivé sur un site auto-hébergé par un bidouilleur amateur. Un assemblage de pages html qui, je l'espère, forme un tout. Point de profilage, ni de ressources extérieures ici. Tout est hébergé `@home`. Le site se veut donc _léger_, _rapide_ et **respectueux du visiteur**.

La partie principale est celle du [**blog**](@/blog/_index.md) où je parle d'informatique, linux, musique et bouquins. Il y a également une partie [**notes**](@/notes/_index.md) où j'essaies, plus ou moins régulièrement, d'y placer de courts textes/mémos sur différentes choses.

Bonne visite et, si cela te plaît, il est possible de venir me déposer un petit truc sur [ma `Mystery Box`](@/mysterybox.md).
