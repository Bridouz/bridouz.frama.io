+++
title = "Un dimanche en EHPAD"
date = 2017-09-24T18:00:00+01:00
[taxonomies]
tags = ["Aidesoignant", "Vie 1.0"]
+++

L'EHPAD est certainement l'endroit qui recrute le plus dans le milieu aide-soignant, il faut dire que la filière est juteuse autant pour les établissements publics que les entreprises privées.  
En revanche ce n'est pas si rose que cela quand on ouvre la boîte...

*Attention ce texte est hautement ironique, les vieux c'est chouette...vraiment*

Nouvelle ville, nouvelle vie, nouveau boulot et me voici donc en EHPAD et principalement sur deux unités de 15 résidents chacune dont une dite *UHR* pour les personnes souffrant de troubles neuro-dégénératifs de type *Alzheimer*.

L'EHPAD est un lieu de vie, considéré comme beaucoup comme une partie de la santé qui n'en est pas vraiment une alors, dans ce cas, pourquoi accorder des moyens humains (et également financiers) pour une catégorie de la société qui a fait son temps, qui ne fera plus gagner d'argent et cela même si elle se lève tôt 7 sur 7 ?

Le dimanche c'est un jour payé un peu plus cher que les autres, que voulez-vous il faut tout de même une petite compensation, alors pour ne pas non plus débourser des milles et des cents les établissements déclarent les dimanches (et jours fériés) comme jours à effectifs réduits. Ce qui concrètement veut dire, dans mon cas, que sur les deux services on retrouve une aide-soignante pour chaque secteur le matin puis une l'après-midi et une autre travaillant en horaire coupé (9h-13h puis 17h30-21h). Dans l'unité *UHR* on est à un ratio de 1 pour 15, une personne qui va devoir en 7h30 de boulot :

  - Donner le petit-déjeuner et les médicaments + faire manger une personne sur les quinze
  - S'occuper de la toilette
  - Assurer le repas du midi + faire manger une personne sur les quinze
  - Installer les personnes faisant la sieste si elle le souhaite
  - *Tenter* de faire un tour pour changer les protections si besoin avant la fin de son service
  - Et tout au long de la journée, elle va devoir surveiller les déambulations, les comportements altérés par la maladie qui se résument par des tentatives de fugues, de la violence physique, des cris, des altercations verbales, du bruit toute la journée.
  
Alors on en arrive à un point ou humainement parlant il est difficile de réaliser *professionnellement* les tâches que j'ai énoncées plus haut. Du coup, au grand dam des soignants, les dimanches riment avec une toilette TMC (Tête-Main-C**), l'essentiel pour que la personne soit un minimum propre. Si des personnes ne peuvent plus marcher et doivent être levées on remettra ça au lundi car le dimanche c'est un course contre la montre épuisante.

Mais il ne faut pas s’arrêter à ce côté *déplaisant* du boulot, qui fait qu'on rentre parfois à la maison avec un sentiment de honte nous suivant comme une ombre. Ce n'est pas un traitement humain pour des personnes méritant simplement qu'on prenne soin d'eux (le prendre soin est tout de même l'essence du métier d'aide-soignante).

S'ajoute à ça les déboires de budget car oui, dans notre vision de la société, le vieux ça ne rapporte plus grand-chose à la tendre *entreprise France* alors pourquoi donc leur allouer des budgets convenables pour leur permettre de profiter de leurs derniers jours après une vie bien remplie. Simple exemple qui suffira à lui-même sur ce budget minimal qui nous est alloué :

  - Certaines personnes, de part leur âge et leur condition physique, ne peuvent tout simplement plus boire de l'eau plate, de liquide. Alors il existe un produit communément appelé *épaississant* qui permet de transformer un liquide en quelque-chose de plus solide, plus épais (vraiment ?) afin d'éviter les fausse-routes (du liquide dans les poumons = pas bon). Hé bien ce produit est rationalisé, nous ne pouvons en commander que deux boites là où pour une semaine il en faudrait 5 ou 6 pour que les personnes puissent être hydratées convenablement. Alors, pour ne pas laisser les gens se *dessécher*, on essaie de donner du liquide, doucement, à la petite cuillère, à la pipette. Le souci c'est que ça prend encore plus de temps, que ça n'apporte pas beaucoup d'hydratation et surtout que généralement la personne ne supporte pas ça et PAF fausse-route assurée et gros moment de frayeur.

Arriver en EHPAD en tant que soignant est une chose délicate, on y entre avec pleins de projets pour un lieu de vie dynamique, un lieu de vie où le résident est soigné, un lieu de vie apaisé et au lieu de ça on rentre dans un lieu où le résident est délaissé, où le soignant doit sans-cesse se battre pour obtenir un peu de dignité dans la prise en soin du résident.

Heureusement que certaines équipes se battent pour leur vision du soin dans un EHPAD car si pour beaucoup le *vieux* est dégouttant et ne donne pas envie d'en faire son secteur de prédilection dans la santé il en reste pas moins qu'il s’agit bien ici d'humain et que nous nous devons tous de faire le nécessaire pour assurer un service de qualité.

S'occuper de résidents dans un EHPAD est une autre facette des métiers de la santé, ce n'est pas comme une hospitalisation. Ici des liens peuvent se créer, on s'attache forcément à force de se voir tous les jours pendant des années. On travail main dans la main avec le résident pour créer un *projet de vie* adapté permettant de mieux le prendre en soin en apprenant à le connaître d'avantage lui et ses goûts, ses envies.  
Le *prendre soin* c'est aussi cela, accompagner au mieux une personne jusqu'à la fin de sa vie, en prenant soin d'ouvrir les yeux pour détecter la moindre anomalie et pouvoir y remédier au plus vite.

J'ose imaginer ce que l'avenir nous prépare car, plus les générations avancent et plus les mentalités changent, les besoins tout autant, par rapport à nos chers *vieux* des années 30-40. Comment allons-nous affronter la chose par la suite avec un tel manque de moyen ? Comment allez-**vous** supporter cela ? Comment allons-nous réussir à ouvrir les yeux et bouger pour essayer de sauver nos derniers jours, nous assurer que des gens seront là avec tous les outils en main pour s'occuper de nous ? Pour nous accompagner à vivre jusqu'au bout et non croupir en espérant être sélectionné pour les quelques sorties trop rarement obtenu par la force par les équipes soignantes ?

Les cartes sont dans nos mains, assurons-nous de les jouer avant de ne plus avoir la force nécessaire et d'abandonner la bataille de l'âge...
