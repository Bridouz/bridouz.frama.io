+++
title = "Blogus Comicus: Fables"
date = 2019-01-07T09:02:00+00:00
[taxonomies]
tags = ["Comics"]
[extra]
song = "[Tool - Sober](https://www.youtube.com/watch?v=hglVqACd1C8)"
+++

Le comics, j'aime le répéter, n'est pas uniquement une affaire de super-héros. Non, en réalité et bien que ces derniers soient les histoires les plus connues de ce médium (notamment grâce à *Marvel* et *DC*) cela ne s'arrête pas qu'à cela et on retrouve beaucoup d'autres choses.

Aujourd'hui j'aimerai vous parler de **Fables**, la création de _Bill Willingham_
et de _Mark Buckingham_. Un série où tous les héros des contes traditionnelles évoluent dans notre monde, à l’abri des regards, devant faire faces aux aléas de la vie ainsi qu'au redoutable _Adversaire_, personnage maléfique ayant conquis les _Royaumes_ et les ayant poussés à s'exiler.

Série parue entre 2003 et 2015 **Fables** conte 21 recueils (TPB) en VO et se voit regrouper dans de magnifiques _Intégrales_ en français par l'éditeur **Urban Comics**. De nombreuse fois primés au prix _Will Eisner_, Fables sort du lot en proposant un récit adulte des héros de notre enfance, nul besoin de connaître les personnages pour comprendre leur comportement, cependant il est intéressant de se renseigner sur les personnages que l'on découvre car il faut bien l'avouer il y en a la pelle. Leur passif est pleinement intégré dans l'histoire et on se prend au jeux de la nostalgie en se ressassant leurs aventures et en riant lors des contentieux entre personnages.

![Fables, Snow et Bigby](/img/comics/fables_snowbigby.jpg)

Niveau graphique la série bénéficie du remarquable travail de _Mark Buckingham_ pour l'histoire principale et d'une multitude d'artistes pour les secondaires. Les dessins sont somptueux, tout comme les couvertures et c'est un plaisir de voir cet univers féerique prendre vie au fil des pages. J'ai un faible tout particulier pour les ornements faisant les bordures des panels, je trouve que cela donne vraiment un charme au tout.

Drames, humour, enquêtes, conflits, ruse et coups dans le dos. Voilà ce qui vous attend dans l'univers de **Fables**. À ce jour (janvier 2019), quatre volumes sont sorties et d'après mes calculs il devrait y en avoir 9 au minimum. Le volume vous coûtera 28 euros pour un minimum de 400 pages (histoire, recherches graphiques, couvertures des singles US). Je recommande chaudement.

![Fables, Intégrale 1](/img/comics/fables_integrale1.jpg)
