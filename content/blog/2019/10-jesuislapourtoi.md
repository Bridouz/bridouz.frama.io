+++
title = "Je suis là pour toi"
date = 2019-05-06T10:00:00+00:00
[taxonomies]
tags = ["Aidesoignant", "Vie 1.0"]
+++

> Billet faisant partie d'un diptyque avec [Je ne connais plus mon nom](@/blog/2019/09-jeneconnaisplusmonnom.md).

Je me lève, 5h. Un café vite avalé, un coup d'eau sur la tronche, un rapide tour du web pour lire deux ou trois informations et hop, dans la voiture direction le boulot. Musique à fond, sinon autant ne pas en écouter, pas le choix de toute façon il faut bien réveiller le bonhomme.

6h30, l'équipe de nuit laisse sa place à l'équipe du matin, les transmissions orales se font.  
6h45, le café est à couler, je prépare le petit-déjeuner pour les résidents.

Ça ronfle ce matin, pas grand monde de lever. Je prépare deux petit-déjeuner et je prends un café avec une résidente. Un résident arrive, pieds nus, en pyjama. Je lui souris, le salut de la main et l'invite à venir prendre le petit-déjeuner. Il arrive face à moi, je lui prépare ses tartines de pains, lui sert un café.  
Il me regarde avec envie, ses mains ne tiennent pas en place, la tentation est trop fort. L'assiette est prête, je l'invite à s’asseoir et savourer ce petit déjeuner. Il me sourit, me dit merci et enfourne un premier morceau de tartine dans sa bouche en disant " Mmmh, oh oui, ça c'est bon."

Je m'occupe d'autres personnes puis, au détour d'une chambre j'entends un résident crier.

> SORS DE MA CHAMBRE ! WHOOOOOOOOO! Y'A QUELQU'UN DANS MA CHAMBRE !

J'y vais, vois le monsieur de ce matin, en train de toucher aux objets sur le meuble TV de l'autre personne, je l'invite à sortir, il ne comprend pas, s'en moque royalement, je lui prends le bras et l'invite alors à me suivre.

Il me fait un sourire puis me suis, m'accompagne dans ma déambulation intentionnelle. Je l'emmène vers sa chambre, ouvre la porte de la salle de bain puis, une fois le nécessaire de toilette sorti, l'invite à se dévêtir pour se laver.  
Comme à son habitude, ce moment le gène au plus au point, il manifeste verbalement son mécontentement. Je lui prends la min et commence à fredonner une chanson qu'il affectionne. Le déclic est là, l'appréhension est mise de côté et je peux alors prendre soin de lui, sans lui faire du mal.

Je sors un chocolat, qu'il prend un malin plaisir à contempler, manipuler, avant de l'envoyer dans sa bouche pour, je l'espère, un bonheur gustatif.

Une fois le soin terminé je m'en vais vers d'autres personnes, en prenant le soin de disposer des morceaux de pain un peu partout dans l'unité, sur des points stratégiques de ses déambulations.

La matinée passe vite et l'heure du repas approche. J'invite les gens à s'installer, repère mon marcheur invétéré au loin en train de … marcher. Je prépare un fauteuil que je pourrais bloquer par la suite pour lui offrir, un peu malgré lui je l'avoue, un temps de repos bénéfique pour lui. Il arrive, s'installe au fauteuil. Je dispose un tapis antidérapant devant lui avec assiette, couverts et verre d'eau.  
Les couverts sont futiles ici, ne servent qu'à plaire à la hiérarchie, car le monsieur n'arrive plus à s'en servir, ne comprend plus comment. Alors l'instinct humain utilise ce qu'il connaît, ses mains. Ce n'est pas dérangeant, le monsieur mange à sa faim, prend du plaisir et porte une serviette de table. Alors pourquoi lui priver ce plaisir.

À la fin du repas je regarde ce monsieur s'assoupir et partir paisiblement dans les bras de Morphée pour quelques minutes. Que c'est bon de le voir calme comme cela, les trais du visage sont détendu et sa panse, quant à elle, bien remplie.

Je pars dans la salle de pause pour les transmissions avec l'équipe d'après-midi. La porte est toujours grande ouverte car il est important de pouvoir offrir des repères aux personnes désorientées. Le monsieur arrive, comme à son habitude. Il regarde avec envies les tasses de café sur la table puis s'avance vers moi, pointe quelque-chose dehors que seul son esprit peut voir et expliquer puis il dit:

> " Ouais... Oh ! Tu vois là, y'a pleins de tibikodu plabsdatroptruc."

Je le regarde, sourie, intérieurement je sais que c'est un appel à l'aide, une demande d'affection masquée. Alors je suis son doigt, regarde à mon tour dehors, hoche la tête pour valider ses émotions, son message. Son visage se détend.

L'heure est arrivée de rentrer à la maison, je me lève, lui tent la main et lui serre avec toute la compassion restante après 8h de travail. Je lui souhaite un bon après-midi et lui dit à demain, il sanglote.

Je le prends dans mes bras, l'appelle par son prénom, le remercie de sa présence. Nos regards se croisent, je crois bien que dans le miens il y a une humidité non dissimulé. Dans le sien, difficile à dire.   Encore une fois il sourit. Un beau sourire, sincère, franc, sans retenu. Un vrai sourire. Il attrape un bout de pain à la volée, avant de partir de la salle.

Je retourne chez moi maintenant, laissant ce monsieur marcher sans interruption, marcher pour vivre, survivre. Je le laisse seul, abandonné, avec 14 autres personnes tout aussi seules les unes que les autres. Je les dépose soigneusement dans mon vestiaire, prends le plus grand soin à bien le fermer pour les garder là, en sécurité.  
Pour pouvoir, jusqu'à demain, passer à autre chose, ne plus y penser, essayer du moins, pour revenir frais et disponible, à l'écoute du moindre bruit, du moindre geste trahissant une souffrance exprimée dans un langage qu'ils sont seuls à connaître, l'espace de quelques minutes, avant d'oublier la clé de décodage et de repartir, en boucle, vers l'inconnu.
