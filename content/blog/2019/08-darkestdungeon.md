+++
title = "Darkest Dungeon, souffrir de jouer"
date = 2019-05-02T10:00:00+01:00
[taxonomies]
tags = ["Jeux"]
+++

Imaginez un jeu où l'objectif est d’enchaîner les donjons par groupe de quatre héros pour, petit à petit, arriver à reconquérir le manoir familial. Facile non ?

Rajoutez donc un peu de stress impactant les héros, leur faisant perdre la boule au point de développer des traits mentaux peu reluisants. Ça commence à se corser ? Ajoutez également la mort permanente aux héros, la reconstruction mentale coûteuse en temps et en argent puis, saupoudrez le tout de mécanismes connus des rpg avec attaques uniquement selon un placement spécifique, malus/bonus et autre résistance/faiblesse, ajoutez aussi une jauge de lumière qui influe sur le tout et vous obtenez [**Darkest Dungeon**](https://www.darkestdungeon.com/). Un bon petit jeu bien addictif et terriblement sadique.

![Darkest Dungeon](/img/jeux/darkestdungeon_banner.jpg)

Je ne suis pas un grand joueur, mais depuis quelque temps j'éprouve l'envie de jouer occasionnellement et si possible à des jeux faits par des studios indépendants, des jeux réfléchis, et durs. Des jeux mettant à l'épreuve ma logique. Celui-ci en fait naturellement partie.

> Il faut tout de même jouer dans un environnement sécurisé si vous êtes comme moi un peu expressif dans la défaite, car avec ce jeu vous allez perdre, beaucoup, souvent, trop selon les goûts. Tout comme les héros virtuels que vous gérez, votre santé mentale sera mise à rude épreuve.

Le principe est simple, vous gérez des héros, reposant dans un petit village où il est possible de les améliorer, de les faire se reposer pour partir à l'aventure. Les quêtes se font par groupe de quatre héros, et durant ces dernières et selon les ennemies que vous rencontrez, les pièges et différentes interactions que vous pourrez avoir ils génèrent du stress.

Ce stress se cote sur 200, une fois passer la centaine votre héro subira une souffrance, un trouble mental néfaste. Ainsi il pourra devenir suicidaire et donc partir au combat sans vous laisser le temps de décider son action ou la refuser, il pourra perdre de la vie, reculer dans l'ordre de vos personnages. Bref selon le trouble un comportement spécifique s'appliquera. Parfois votre héros ne souffrira pas, bien au contraire, il obtiendra une vertu offrant alors un bonus pour le personnage.

Si le stress atteint son seuil maximum le personnage subira une crise cardiaque, réduisant ainsi sa jauge de vie au néant en le faisant entrer en agonie. Là deux cas de figure se présentent, soit le personnage succombe, soit il est en sursis et chaque attaque risquera de l'anéantir, de plus des pénalités de stats viendront s'ajouter.

![Darkest Dungeon Affliction](/img/jeux/darkestdungeon_affliction.jpg)

Cet aspect du jeu est vraiment intéressant, il humanise les personnages en leur donnant des traits propres ainsi l'aventure se corse, car il faudra veiller à toujours associer environnement, quête et caractères. Et parfois le hasard fera que même un bon personnage pourra tomber en quelques combats, vous emportant avec lui dans un profond chagrin.

Des bosses vous attendent régulièrement pour venir corser les choses et, selon les quêtes il est possible de récupérer des objets pour vos personnages en récompense. Ces objets offrent souvent des avantages précieux pour les statistiques et, bien souvent, un malus viendra également avec l'objet forçant le joueur a bien réfléchir à sa stratégie.

Le jeu est sorti en janvier 2016 et compte à ce jour 4 dlc venant apporter des lieux, ennemies, héros, et mécanismes nouveaux. Pour le moment je n'y ai pas touché et suis l'aventure basique. Si vous avez envie de voir quelques images du jeu, leur site permet de se faire un avis avec [un bon nombre de capture d'écran](https://www.darkestdungeon.com/media/#screenshots).

**Darkest Dungeon** est donc un jeu éprouvant pour le joueur, l'impliquant dans son aventure, l'invitant à vivre des moments difficiles comme des moments de joie (obtenir une vertus lorsque le stress atteint les 100 points, c'est toujours un grand moment de soulagement).

![Darkest Dungeon Virtue](/img/jeux/darkestdungeon_virtue.jpg)
