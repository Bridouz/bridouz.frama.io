+++
title = "Musique - Mars 2019"
date = 2019-03-27T10:15:00+01:00
[taxonomies]
tags = ["Musique", "Fusion", "Jazz", "Folk"]
+++


Le troisième mois de l'année 2019 touche bientôt à sa fin, autant le finir comme il se doit. En musique.

## Snarky Puppy - Immigrance

![Snarky Puppy - Immigrance](/img/musique/snarkypuppy_immigrance.jpg)

Nouvel album, nouvelle claque. Comme toujours Snarky Puppy est un groupe doté d'une virtuosité impressionnante. Moins de démonstration technique sur cet album, au contraire le groupe joue beaucoup plus sur l'instauration d'ambiances et une musique de groupe cohérente où chaque instrument est à sa place, mettant en avant le collectif. D'ailleurs les morceaux ont été écrits à bien plus de mains qu'à l'ordinaire et cette richesse d'écriture se ressent tout au long de l'album. On peut y déceler par-ci par-là les _sans-doutes_ influences de tel ou tel musicien.

Taillé pour le live, qui je pense pourra offrir une dimension supplémentaire voir complètement nouvelle aux titres, l'album ébloui par son exploration renouvelée. Mention spéciale au dernier morceau, _Even Us_, petite pépite tirant son aura d'influences d'Europe de l'Est et du Moyen-Orient.

Encore une fois le groupe livre un album de toute beauté qui, au fil des écoutes, n'en finit toujours pas de révéler ses petits secrets, ses notes disséminées l'air de rien. Une réussite qui me tarde d'apprécier encore plus en live.

[Snarky Puppy - Chonks](https://www.youtube.com/watch?v=tmeJsETS-Yk)

## Weend'ô - Time of Awakening

![Weend'ô - Time of Awakening](/img/musique/weendo_timeofawakening.jpg)

Un groupe français de rock progressif découvert il y a peu grâce à l'excellent [musicwaves](http://www.musicwaves.fr). Une voix féminine poétique et rafraîchissante, des musiciens sachant laisser place aux silences, ne jouant pas tous leur partie à toute vitesse. On sent ici la grande maîtrise du rythme, ça groove pas mal et c'est un plaisir pour les oreilles. Là aussi on trouve cet équilibre si précieux entre tous les membres du groupe, personnes ne prend le pied sur l'autre et cela offre un album hypnotisant, magistralement exécuté.

Une découverte vraiment plaisante, qui sort de tous ces groupes se ressemblant les uns aux autres.

[Weend'ô - Time of Awakening](https://www.youtube.com/watch?v=e7MJSTPRJDk)

## David Crosby - Here If You Listen

![David Crosby - Here If You Listen](/img/musique/davidcrosby_hereifyoulisten.jpg)

Le monsieur n'est plus a présenté, véritable dieu vivant du rock/folk américain David Crosby a connu les sommets puis le vide total avant de resurgir de nul part en 2014 avec son album Croz, le premier d'un enchaînement merveilleux. Âgé de 77 ans, le musicien propose ici son septième album solo, toujours dans un style très épuré, envoûtant, et terriblement musicalement parfait.

_Here If You Listen_ est donc son dernier album en date, écrit lors de la tournée du précédent (_Lighthouse_) et composé majoritairement à quatre ( avec Becca Stevens à la guitare, Michael League (Bassiste/Leader de _Snarky Puppy_, Crosby étant sur le label du groupe)  et Michelle Willis au clavier), l'album est un exemple de cohérence, de partage. Point de chagrin ici, l'ambiance est joyeuse et empli d'espoir. On se laisse happer avec grand plaisir dans ce bonheur hors du temps que nous offre ce vieil homme qui, ayant déjà tout vécu (lisez sa biographie, le monsieur a eu plus de vies qu'un chat.), joue pleinement sa musique, sans se soucier de quoi que ce soit d'autre. Comme toujours la voix de _Crosby_ est fascinante et le travail fait sur les harmonies vocales force le respect tant l'émotion qui s'en dégage est puissante.

Voilà pour ce mois de mars 2019, et celui qui arrive s'annonce plein de bonnes choses.

[David Crosby - Woodstock](https://www.youtube.com/watch?v=Lp5iJGOHOlY)
