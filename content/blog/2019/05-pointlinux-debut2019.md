+++
title = "Point Linux, début 2019"
date = 2019-02-13T18:00:00+01:00
[taxonomies]
tags = ["Linux", "Solus", "Gnome", "Kde"]
[extra]
song = "[Gojira - Global Warning](https://www.youtube.com/watch?v=8DiWzvE52ZY)"
+++

Début d'année 2019 et voici un point sur mon utilisation actuelle de l'informatique et de Linux.

Niveau distributions je tourne toujours avec [Solus](https://getsol.us). Toujours comblé par la stabilité et la rapidité du système. L'équipe a d'ailleurs publié en début d'année une [_roadmap_](https://getsol.us/2019/01/14/2019-to-venture-ahead/) pour 2019, beaucoup de bonnes choses à venir.

**Budgie** est mon DE de prédilection, les logiciels _GNOME_ sont intuitifs, sans superflue et j'aime cela. Ça va droit au but, permet de faire simplement les actions de tous les jours d'un utilisateur moyen. J'attends le passage à _GNOME 3.30 / 3.32_ pour voir si le gain de réactivité apparemment apporter par ces versions s'appliquera avec **Budgie**.  
Je continue à suivre _KDE_ de près car à chaque nouvelle version le DE devient de plus en plus alléchant, l'équilibre entre  une multitude de fonctionnalités, des réglages par défauts adaptés à l'utilisateur lambda et une possibilité de personnalisation accrue font que _KDE_ devient réellement intéressant.

La distribution (Solus) est maintenue par une petite équipe de passionnés, des gens œuvrant à apporter un environnement propre et agréable à utiliser. Pas le moindre bug en deux ans d'utilisation, une maj hebdomadaire le vendredi et c'est un plaisir de réactivité. Le départ du fondateur/développeur principal durant l'été 2018 aurait pu totalement chambouler **Solus** mais Josh, Bryan, Peter, Jean-Yves et d'autres ont admirablement repris le flambeau. Certes le parlé et l'hyperactivité d'_Ikey_ manqueront au paysage Linuxien, il laisse tout de même une création, une vision et c'est déjà beaucoup.

Terminé donc la bidouille et la multitude de logiciels, retour à l'essentiel.

  - Le vénérable **Firefox** comme navigateur web
  - **Gedit/LibreOffice** pour l'édition et le traitement de texte
  - **Lollypop** comme lecteur audio
  - **Transmission** pour le torrent
  - **Syncthing/Nextcloud** pour les sauvegardes et flux _rss_
  - **Gnome-mpv** pour la lecture vidéo
  
Finalement je n'ai pas vraiment besoin de plus, tout est fonctionnel, avec une prise en main relativement facile.

Niveau blog après un bref passage avec [ssg4](https://www.romanzolotarev.com/ssg.html) je reviens avec [hugo](https://gohugo.io) (depuis ce billet). C'est complet, ça marche et surtout j'aime vraiment l'utiliser, j'y ai mes habitudes et ça tourne sans soucis. Le thème se veut minimaliste, utilise le _framework_ css [**sakura**](https://github.com/oxalorg/sakura) avec de légères modifications, un thème propre, simple et agréable.  
L'auto-hébergement est toujours de mise, pas d'impact majeur sur la bande-passante à la maison, le déploiement est assuré via _git_ et un _hook_ assurant la génération automatique à chaque push. Pour ce qui est des statistiques je n'y prête que très peu d’intérêt, j'utilise cependant [_goaccess_](https://goaccess.io/) pour regarder parfois les visites sur la journée.

Finalement _Linux_ est, je pense, vraiment devenu agréable à utiliser pour quiconque veut l'utiliser. Que ce soit avec _GNOME_ ou _KDE_, les deux poids lourds niveau DE font le taff amplement, facilitent l'utilisation classique d'un ordinateur.   Les distributions les plus connues sont vraiment stables et l'offre de logiciels (tout comme leur qualité générale) s'est grandement étoffée.  
Que c'est bon d'être libre..
