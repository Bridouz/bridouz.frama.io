+++
title = "Vivre avec le reste ou la distance professionnelle"
date = 2016-11-13T18:00:00+01:00
[taxonomies]
tags = ["Aidesoignant", "Vie 1.0"]
+++

Voilà maintenant près de 4 mois que je suis professionnel et outre le mode de vie qui change, les tâches administratives qui pointent le bout de leurs nez prouvant bien que je suis définitivement rentré dans le monde des grands à mon plus grand désespoir sur ce sujet (c'est d'un chiant l'administratif), le taf me fait pas mal cogiter et pour cause, je suis souvent confronté à des situations singulières, des situations d'urgences vitales.

Premièrement il m'aura fallu deux mois pour me défaire de cette barrière mentale qui me faisait encore penser que jetais un stagiaire. Dorénavant je pense être en mesure d'être un professionnel, qualifié, faisant relativement bien son boulot même si le chemin de l'apprentissage ne disparaîtra jamais et c'est tant mieux.

Depuis septembre je travaille dans un service de soins intensifs de cardiologie, autant dire que parfois ça peut aller très vite et qu'il faut être en mesure de réagir rapidement et proprement face à des situations mettant la vie de personnes en jeu. Du haut de mes vingt-quatre années mon temps de réaction est encore assez faible par rapports à des collègues aguerris et ces situations laissent une marque indélébile dans mon esprit.

Une fois la journée de travail terminée, je raccroche la tenue *- je la balance dans le bac à linge plus précisément, mon adresse au shoot commence à afficher des statistiques plutôt satisfaisants d'ailleurs -* et je rentre chez moi pour reprendre une vie privée. Du haut de mes vingt-quatre années je dois savoir dissocier le boulot du reste car sinon c'est un beau bordel. Au taff les soignants sont souvent confrontés à des situations atypiques que ce soit sur les histoires de vie de certains patients, sur l’état physique ou émotionnel de ces derniers et bien sûr les situations urgences vitales où la vie ne tient plus que par un fil. Si la différentiation *travail/le reste* je ne peux la faire, je vois mal comment vivre avec tout cela sans que ma vie privée en pâtisse.

Se retrouver entouré de médecins et d'infirmiers entourant eux-mêmes un patient en arrêt cardiaque n'est pas une situation qu'on vit tous les jours. Savoir quoi faire alors que l'on est encore jeune diplômé, sans recule, sans expérience est déstabilisant. Et, quand la mort pointe le bout de son nez elle amène avec elle un tas de questions et un chamboulement.

Je me retrouve face à un être sans vie, où mon travail consiste à assurer l'après, tout ce qui doit être fait avant que la famille arrive, l'après-vie. Se retrouver, à vingt-quatre ans, devant un homme que l'on a vu mourir devant soi, que l'on a pu avec l'équipe médicale sauver est une situation assez étrange. Tout a été fait pour le réanimer et l’échec doit être accepté, digéré pour pouvoir continuer à avancer, à travailler.

Dans des moments comme celui-ci il est important de travailler en équipe, pour se soutenir, parler, et avancer mais, je trouve qu'il est également important - voir même essentiel - de faire un travail sur soi. Ce jour-là en revenant chez moi, le trajet du retour me parut cours, et je n'ai pensé qu'à cette situation une fois à la maison. J'en ai oublié ma vie privée l'espace de quelques-heures et c'est ici que le danger se trouve. Se perdre soi-même à cause du boulot.

Cette distance professionnelle n'est pas quelque-chose d’innée et elle évolue avec le temps, avec ce que l'on vit aussi bien au boulot comme à la maison, elle peut parfois être fine, voir même quasi inexistante en apparence et pourtant elle est bien là. Le fait de rire d'une situation dramatique n'est pas un manque de respect non, cela montre la distance que le soignant met entre le boulot et lui-même pour ne pas s'autodétruire.

Pour le moment, et c'est une très bonne chose, je n'ai été directement confronté à la mort qu'à deux reprises. Je ne fais pas ce métier pour voir des gens mourir, et même si ce fait doit être assimilé pour travailler dans ce domaine je pense qu'aucun soignant ne souhaite voir beaucoup de décès dans sa carrière car, même si parfois ils sont inévitables, ils remettent tout un tas de chose en question dans le cadre d'un exercice professionnel et viennent profondément bouleverser le soignant et l'équipe soignante.

En relisant le billet je me rends compte qu'il est assez brouillon, et ça reflète bien la cogitation actuelle dans mon cerveau. Avec la mort rien ne peut être arrêté, la théorie n'éxiste pas finalement, et chaque situation m'affectera singulièrement, nous affecte d'une empreinte qui lui est propre. Alors il faut avancer, y penser, ne pas oublier, mais avancer pour continuer à faire son boulot et vivre avec le reste, ne pas céder, prendre de la distance et avancer, toujours. 

Pouvoir avancer avec soi-même on appelle ça la résilience dans la langue psychologisante.
> La résilience est un phénomène psychologique qui consiste, pour un individu affecté par un traumatisme, à prendre acte de l'événement traumatique pour ne plus, ou ne pas, avoir à vivre dans la dépression et se reconstruire.

Alors oui il est parfois difficile de rentrer chez soi et mettre de côté les émotions vécus au travail, il est parfois difficile de rentrer chez soi et de continuer à vivre comme on l'a toujours fait en ayant conscience de certaines situations. Mais il est impératif de continuer à vivre et oublier, l'espace de quelques heures, le boulot et ce qu'on y voit. Tout cela construit cette distance, construit le soignant que l'on deviendra.

