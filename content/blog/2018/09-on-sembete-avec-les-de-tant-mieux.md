+++
title = "On s'embête avec les DE, tant mieux"
date = 2018-09-28T11:00:00+01:00
[taxonomies]
tags = ["Linux"]
+++

J'ai pu lire chez [Olivyeahh](https://leblogdolivyeahh.wordpress.com/2018/09/23/sur-les-desktops-daccord-pas-daccord/) et [Frederic Bezies](http://frederic.bezies.free.fr/blog/?p=17949) deux billets fort intéressants sur la stabilité qu'offrent les DE de nos jours et le fait qu'il y est donc une perte de _bidouille_ pour l'utilisateur. Car, il faut bien le reconnaitre, le monde linuxien est devenu beaucoup plus agréable à manier pour un utilisateur standard qu'il y a quelques années.

Les distributions s'installent majoritairement sans encombre, il faut cependant se familiariser avec les images `iso` et comment les mettre sur un support usb. Le _Live CD_ n'éxiste quasiment plus de nos jours, 700MO pour une distribution c'est un peu dépassé (ou alors c'est une _netinstall_).<br />
Donc l'installation se fait sans soucis et arrive par la suite le choix de **DE**. Il est vrai que pour un utilisateur windowsien n'ayant pas eu le choix auparavant sur son environnement de bureau il y a de quoi être perdu lorsqu'on commence les recherches sur Linux. _Gnome_, _Kde_, _Xfce_, _Mate_ ou encore _Cinnamon_ pour les plus connus, il y a du choix et on ne voit pas très bien ce que l'un apporte par rapport à l'autre.

Choisir _GNOME_ permet d'avoir une interaction visuelle avec son OS d'une façon nouvelle. Déroutante la première fois, mais qui peut s'avérer très intéressante si on y adhère rapidement. La simplification est de mise et on se rend compte rapidement si cette dernière nous convient ou non.

Choisir _Kde_ permet d'avoir un DE solide et plus que complet, un environnement qui est capable de s'adapter à l'utilisateur. Les moins exigeants accepteront les réglages par défauts et retrouveront des habitudes visuelles prise chez Windows. En revanche si le besoin de régler le **DE** plus précisément se fait sentir il est tout à fait possible de le faire. _Kde_ permet une profondeur de modification assez intéressante sans pour autant pousser l'utilisateur à tout régler. Si ce dernier en a envie la possibilité est là, le cas contraire tout fonctionnera sans rien toucher.

Choisir _Xfce_ revient aussi à choisir la simplicité, la légèreté, dans un environnement visuel ne cassant pas les codes windowsiens. Son apparence ancestrale à première vue peut en détourner plus d'un mais une fois le tour des configurations possibles il se peut que sa légéreté vous convienne. Son rythme lent de mise à jours en fait un OVNI dans le paysage actuel où l'on veut un flux continue de nouveautés. Ici rien ne change en apparence, dessous le capot les changements sont lents et ne seront publié qu'une fois le tout refait à neuf.

Tout l'écosystème Linux est quand même plus plaisant à utiliser qu'il y a quelques années. Globalement ça fonctionne sans rien modifier et les utilisateurs moins chevronnés y gagnent en maniabilité.

Si je prends l'exemple de madame à la maison, elle tourne depuis un an sous Linux maintenant et elle n'a eu qu'une fois à aller bidouiller (pour utiliser flash car une web app pour son boulot fournit avec). Sinon tout a fonctionné comme sur des roulettes. Le navigateur tourne, LibreOffice ne plante pas, elle peut écouter de la musique, télécharger des torrents et ranger ses fichiers avec le gestionnaire. C'est majoritairement ce que font les utilisateurs standards avec leur ordinateur, et ça marche au top.

Sans nul doute une étape est franchie. Plus besoin d'éditer des fichiers `.conf` pour faire détecter son imprimante (la faire fonctionner était encore autre chose il y a quelques années), on branche et soit le driver s'installe seul soit il suffit d'ouvrir les paramètres du système pour l'installer tranquillement. Les logiciels _basiques_ sont d'une solidité impressionnante, et le l'éventail de choix est là pour satisfaire la majorité des utilisateurs.<br />
D'un point de vue stabilité et confort d'utilisation c'est une victoire. Avoir un environnement graphique stable c'est également pouvoir se concentrer sur autres choses. Les développeurs de ces derniers l'ont bien compris et se focalisent maintenant sur l'amélioration des performances, l'optimisation dans la gestion des ressources afin de pouvoir s'adapter à la multitude de configurations possible sur un PC.

La ligne de commande n'est pas pour tout le monde, il faut un minimum de connaissance pour ne pas casser quelque-chose et il faut surtout de la volonté. Chose que certains n'ont pas et n'en veulent pas car voulant faire autre-chose de leur outil informatique et c'est leur droit.
Ceux voulant apprendre le feront naturellement, et ceux en voulant toujours plus et aimant la _bidouille_ se tourneront vers d'autres solutions comme les gestionnaires de fenêtre _tiling_. Il y en a pour tout le monde et c'est ça qui est chouette avec le monde Linux.

Linux est vraiment devenu **grand-public** au sein de son écosystème, il manque seulement le changement dans la mentalité générale pour qu'un nombre d'utilisateurs _pré-disposés_ à pouvoir utiliser Linux franchissent le pas. Et c'est cela sans doute qui sera la prochaine grosse étape, impliquant davantage de vulgarisation et davantage d'échanges humains pour faire connaitre et reconnaitre Linux comme une alternative viable, sécurisé et complète.
