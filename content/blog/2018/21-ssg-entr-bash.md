+++
title = "Ssg4, Entr et Bash Blogging"
date = 2018-12-27T09:00:00+01:00
[taxonomies]
tags = ["Linux", "Outils"]
[extra]
song = "[Steven Wilson - Arriving Somewhere But Not Here (Porcupine Tree)](https://www.youtube.com/watch?v=9TkGUbGTzoU)"
+++

Ouais, encore un changement, probablement le dernier pour cette année. En fait je dois bien avouer que cela faisait longtemps que je lorgnais du côté des générateurs de sites statiques relativement simple, notamment avec [swx](https://3hg.fr/Scripts/swx/).

Et puis en faisant des recherches je suis tombé sur [ssg](https://www.romanzolotarev.com/ssg.html) qui est actuellement dans sa quatrième itération.

> Make a static site with find(1), grep(1), and lowdown(1)

Et là forcément ça me donne envie, un site entièrement généré avec des outils inclus d'office dans toutes les distributions Linux/BSD hormis `lowdown`, mais ce dernier se compile en une fraction de secondes et ne dépend quasiment de rien.

De la légèreté au rendez-vous donc et c'est effectivement le cas. Ici pas de système de *tags*, *frontmatter*, *redimentionnement d'images* ou de *templating html*. Ici nous sommes sur du basique, une génération brute d'un fichier `markdown` en un fichier `html` en entourant le contenu avec un fichier `header.html` et un fichier `footer.html`.

En fait `ssg` est tellement léger qu'il n'y a même pas de génération d'archives pour les billets, de page d'accueil avec ces derniers ou encore d'un flux rss (qui peut être généré avec un outils du même auteur [rssg](https://www.romanzolotarev.com/rssg.html))

Au départ je m'étais dit que j'allais tout faire à la main et puis, comme à l'habitude, j'ai changé d'avis et j'ai retroussé les manches pour pondre un truc bien crado qui fait le job. Le script génère donc une page d'archive avec l'ensemble des billets présents dans le dossier `posts`, il affiche les cinq derniers billets sur la page d'accueil et enfin il régénère le `footer` en incrustant la date et l'heure de génération.

Avec cela j'ai également découvert l’outil `entr` qui permet de surveiller des changements dans un répertoire pour ensuite y exécuter une commande.

```bash
#!/bin/sh
cd dst && python3 -m http.server &
cd $HOME/Blog
while :
do
    find . -type f ! -path '*/.*' |
    entr -d "$HOME/Blog/exssg"
done
```

Ce minimalisme me eprmet d'essayer de bidouiller en scripts `bash` et avec 229 lignes de code pour `ssg4` et 184 pour `rssg` il y a vraiment de quoi s'amuser pour un non professionnel.

Aurais-je finalement trouver chaussure à mon pied ? Il se pourrait bien que oui.
