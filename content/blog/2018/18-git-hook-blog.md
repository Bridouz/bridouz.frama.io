+++
title = "Git-hook et blog"
date = 2018-12-17T18:30:00+01:00
[taxonomies]
tags = ["Zola", "Trucs"]
+++

Le procédé de publication d'un blog ne doit pas être fastidieux 
sous peine de gâcher l'écriture et son plaisir. J'utilise **Zola** comme générateur statique pour mon blog et le tout est versionnée avec *git*. Du coup à chaque commit envoyé sur mon serveur le script suivant est exécuté.

```
#!/bin/bash
    
export GIT_WORK_TREE=/home/git/blog
    
echo `pwd`
echo "Generating site with Zola"
    
mkdir -p $GIT_WORK_TREE
chmod 755 $GIT_WORK_TREE
    
git checkout -f master
    
rm -rf $GIT_WORK_TREE/public
cd $GIT_WORK_TREE && zola build
cd $GIT_WORK_TREE/public && ln -s rss.xml index.xml
find $GIT_WORK_TREE/public -type f -print | xargs -d '\n' chmod 644
find $GIT_WORK_TREE/public -type d -print | xargs -d '\n' chmod 755
    
 echo "All done!"
```

Il supprime le blog et me régénère, applique les bonnes permissions et rajoute un lien symbolique vers l'ancienne adresse du flux rss histoire que ça ne casse pas tout à chaque changement de générateur.

Il est possible de faire cela avec des forges comme *Gitlab* ou *Gitea* tout comme il est possible de le faire en utilisant **git** sans ces interfaces.  
Pour cela il suffit de créer un fichier `post-receive` dans `$VOTRE_DÉPÔT_GIT/hooks/` et le tour est joué.

Écriture, commit, push et basta.
