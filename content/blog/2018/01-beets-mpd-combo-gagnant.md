+++
title = "Beets et MPD, un combo gagnant"
date = 2018-06-06T17:45:00+02:00
[taxonomies]
tags = ["Musique", "Linux", "CLI", "Python"]
+++

Depuis quelques années j'accumule les albums en mp3 sur mon PC, ma médiathèque grossie petit à petit et il est important de garder des informations à jours dessus, histoire de pouvoir savourer pleinement les écoutes. En effet il est toujours agréable de connaître l'année de sortie (originale) d'un album, l'équipe et les musiciens ayant travaillés dessus. J'éprouve ce besoin surtout pour les albums de rock des années 70 comme *Pink Floyd*, *Rush* et autres *Yes* et *Led Zeppelin*. Le **padre** est bien là en cas de besoin, pour sortir des anecdotes sur le sujet... Et MusicBrainz également.

[**MusicBrainz**](https://musicbrainz.org/) C'est un site où les sorties sont répertoriées et les tags id3 à jour.

Avec plus de 300 albums (Buckethead m'en prends plus d'une centaine à luio tout seul) la tache manuelle avec des logiciels comme EasyTag pourrait s'avérer fastidieuse, mais le monde de l'informatique a souvent une solution plus automatisée et pour la musique elle s'appelle [**Beets**](http://beets.io/).

> The purpose of beets is to get your music collection right once and for all.  
It catalogs your collection, automatically improving its metadata as it goes using the MusicBrainz database.  
Then it provides a bouquet of tools for manipulating and accessing your music.  
Because beets is designed as a library, it can do almost anything you can imagine for your music collection. Via plugins, beets becomes a panacea.

L'outil est écrit en **Python**, et donc installable via `pip install beets`. Une fois installé il est accessible via la commande `beets` ou `beet` dans un terminal.  
Ensuite il suffit de lancer un `beet import` dans le dossier contenant vos fichiers musicaux pour que le logiciel se mette au travail comme un grand, allant chercher les bonnes informations sur le net et vous avertissant que plusieurs choix sont possibles quand les informations de vos fichiers ne sont pas assez précises. Notons qu'outre *MusicBrainz*, `beets` permet aussi de lancer des recherches sur *Discogs* ainsi que sur *Beatport*.

Son atout principal reste la gestion d'une médiathèque par ses méta-données, mais `beets` ne s’arrête pas là et il propose également de transcoder vos fichiers dans le format de votre choix, de gérer les doublons dans votre médiathèque ainsi que d'identifier les albums ayant des pistes manquant à l'appel.  
S'ajoute à cela la possibilité d'avoir un lecteur web *HTML5* pour contrôler sa musique depuis son navigateur.

Pour ma part la configuration que j'ai pour `beets` ressemble à ça, j'ai deux dossiers contenant de la musique, un *pending* où je stocke les nouveautés à écouter, et lorsque l'album me plait je l'importe avec `beets` et les fichiers sont modifiés, ordonnés puis déplacés dans le répertoire *Musique*:

``` yaml
directory: ~/Musique
library: ~/.config/beets/library.db

import:
  move: yes

match:
    preferred:
        media: ['CD', 'Digital Media|File']
        original_year: yes

plugins: fetchart inline play random
paths:
    default: $albumartist/$year - $album%aunique{}/%if{$multidisc,Disc $disc/}$track $title
item_fields:
    multidisc: 1 if disctotal > 1 else 0
art_filename: cover
threaded: yes
original_date: yes

play:
    command: mpc add
    raw: yes
```

Vous l'aurez compris, `beets` est un couteau suisse pour amateurs de musique et d'organisation. Si vous êtes déjà un adepte de la ligne de commande, je pense que vous allez vite être conquis. Pour les autres, ce ne sera qu'une question de temps pour s'habituer à l'outil, mais je doute que vous mettiez longtemps vu la simplicité d'utilisation.

Bonne écoute.

---
<https://musicbrainz.org/>  
<http://beets.io/>
