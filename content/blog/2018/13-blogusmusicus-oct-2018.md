+++
title = "Musique - Octobre 2018"
date = 2018-11-12T15:20:00+01:00
[taxonomies]
tags = ["Musique", "Soul", "Rock", "Métal", "Prog"]
+++

Avec un peu de retard voici la fournée musicale du mois d'octobre 2018. Trois albums dont deux nouveautés.

## Con Brio : Explorer

![Con Brio - Explorer](/img/musique/conbrioexplorer.jpg)

L'arrivée du froid laisse plus de place à l'écoute de musique. Moins de temps dehors, plus de temps au chaud avec le casque sur la tête.

Le groupe revient après un premier album d'une incroyable qualité et une notoriété de groupe live acquise à juste titre. Explorer est le nom de ce deuxième opus et il porte bien son titre tant, à la première écoute, l'univers musical soûl/funk du groupe parait changé.

Le son se veut plus _moderne_, dans l'air du temps avec pas mal d'électronique et les mélodies ont un aspect plus pop et normé.

**Con Brio** ne perd pas pour autant son charme du premier album et on ressent tout au long de l'écoute l'influence sous-jacente qui n'est jamais loin. Rien que sur le premier morceau _Saddle Up_ les premières notes de guitares nous mettent dans le bain et les cuivres finissent le boulot, accrochant définitivement l'auditeur pour un voyage musical.

_Heart shaped box_ est une reprise de grande classe, changeant totalement le spectre du morceau originel de **Nirvana**. Les derniers morceaux reviennent vers du funk/soûl comme on pouvait en trouver sur le premier album.

*Explorer* porte bien son nom et nous emmène dans un univers inhabituelle pour le groupe. Pari réussi et les prestations live s'annoncent très intéressantes tant le groupe déploie tout son potentiel lorsqu'il est sur scène.

[Con Brio - Too Lit 2 Quite // Live at North Sea Jazz Festival](https://www.youtube.com/watch?v=l8ghOprNhrA)


## The Pineapple Thief : Dissolution

![The Pineapple Thief - Dissolution](/img/musique/thepineapplethiefdissolution.jpg)

Groupe totalement inconnu et pourtant il s'avère être actif depuis 1999 avec un bon nombres d'albums sortis.

Du rock progressif dans la veine de *Steven Wilson*, c'est calme et mélancolique. Le son est très bien travaillé, mention spéciale pour les guitares qui, il faut le signaler, ont ici trouvé le juste milieu entre distortion et netteté.

Pas de structures complexes ici, ni même de multiples nappes confuses d'instruments. Non, avec *Dissolution* le groupe propose une musique épurée, accessible bien que progressive.

[The Pineapple Thief - Try As I Might](https://www.youtube.com/watch?v=-yBCcT3L2e0)

## Gojira - The Way of all flesh

![Gojira - The Way of all Flesh](/img/musique/gojirathewayofallflesh.jpg)

Recemment j'ai fait le ménage dans la musicothèque sur mon PC et en nettoyant les artistes et albums qui ne m'intéresse plus je suis retombé sur **Gojira**. Fleuron du métal français le groupe produit un death métal moderne où il ne suffit pas de frapper comme un malade sur sa batterie et de balancer des riffs le plus rapidement possible en
hurlant comme un dératé.

_The Way of All Flesh_ est l'album qui m'a fait découvrir le groupe, je crois d'ailleurs que j'avais écouter un morceau dans un CD demu magasine Hard Rock.Celà faisait très longtemps que je n'avais pas écouter cette galette et c'est toujours un plaisir.

Le son est puissant, travaillé tout en laissant pourtant surgir cette brutalité sans artifice. Les compositions sont d'une qualité impréssionnante, fleurtant parfois vers un côté progtessif que je ne saurais regnier. Le jeu de batterie est à couper le souffle, à mille lieux des stéréotypes du métal. Tantot tribal, tantot mechanique, le tempo est donné et c'est un album que je vous conseille vivement.

[Gojira - Oroborus](https://www.youtube.com/watch?v=2o_MPASoLC4)
