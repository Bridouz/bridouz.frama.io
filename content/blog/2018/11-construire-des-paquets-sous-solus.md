+++
title = "Construire des paquets sous Solus"
date = 2018-11-04T23:00:00+01:00
[taxonomies]
tags = ["Solus", "Contribution"]
+++

Construire des paquets pour une distribution permet aux utilisateurs de pouvoir télécharger des logiciels prêts à l'emploi, de ne pas avoir à les construire et reconstruire. Sous Solus le packaging est relativement simple et chacun peu y participer, soumettre des mises-à-jour ou des demandes d'inclusion de logiciels dans les dépôts. (Les membres de l'équipe intégreront vos maj et valideront ou non les demandes d'inclusion)

## La préparation

Pour pouvoir commencer à construire des paquets il faut préparer le terrain, installer les bases qui vont nous servir.

### Le fichier packager
Premièrement il est important de créer un fichier `packager` dans le dossier `~/.solus`. Ce fichier contient votre nom et votre adresse mail, qui seront ajouté au paquet.

```
[Packager]
Name=Votre nom
Email=votre.adresse@mail.truc
```

### Solbuild

Par la suite il faut installer les outils de développement nécessaire avec la commande `sudo eopkg it -c system.devel`, veillez également à ce que git soit installé.

#### Initialiser solbuild

Il suffit alors d'installer **solbuild** avec `sudo eopkg it solbuild`. Ensuite deux manières de construire les paquets s'offre à vous.

  - Construction en se basant sur les paquets et versions de logiciels disponibles dans *shanon*, la branche stable, celle que la majorité des utilisateurs utilisent.
  - Construire en se basant sur *unstable* et donc bénéficier des dernières versions logiciels.

Il est important de noter que les MAJ chez Solus se font généralement le vendredi, donc il est possible en construisant avec la branche stable que des versions de logiciels empêchent la création d'un paquet.

En revanche la construction d'un paquet avec stable permet de pouvoir l'installer sur le système directement après la construction alors qu'en le créant avec unstable il faudra parfois attendre le sync du vendredi si les versions de logiciels diffèrent entre les deux branches.

Si vous choisissez de construire avec unstable il faudra également installer le paquet `solbuild-config-unstable`.

Une fois cela fait la commande `sudo solbuild init`peut être lancé et l'image de base de solbuild va être construite. Profitez-en pour aller vous prendre une tasse, un bol, un verre, ou vous faire un irish coffee.

#### Mettre à jour solbuild

Solbuild est une image à un moment t des logiciels constituant la base du système de construction de paquets et donc, les versions des logiciels peuvent changer. Ainsi il est conseiller de mettre à jour solbuild régulièrement pour ne pas devoir à chaque construction télécharger les dites-mises à jour. La commande `sudo solbuild update` fera le travail.

### Common

Pour finir il faut télécharger `common`, un ensemble de scripts facilitant l’empaquetage. Le paquet `git` doit être installé sur votre système pour cela et `common` doit être téléchargé dans le même dossier ou vous allez empaqueter (chaque empaquetage se fera dans un sous-dossier portant le nom du logiciel).

``` bash
git clone https://dev.getsol.us/source/common.git
```

Ensuite nous créons des liens symboliques pour permettre l'accès à ces fichiers de construction.

``` bash
ln -sv common/Makefile.common .
ln -sv common/Makefile.toplevel Makefile
ln -sv common/Makefile.iso .
```

Dans le dossier de votre futur paquet il faudra utiliser `echo "include ../Makefile.common" > Makefile` pour permettre de lancer la construction du paquet.

Voilà donc à quoi ressemble la hiérarchie de mon dossier nommé `Solbuild`.

```
| common/
| your-package/
| - Makefile
| Makefile
| Makefile.common
| Makefile.iso
```

## Créer des paquets

Enfin ! Eh bien non, il y a encore quelques bases à voir avant de se lancer. L’Irlande ne s'est pas construite en un jour.

### Package.yml

Le fichier concentrant les éléments relatifs à la construction d'un paquet se nomme `package.yaml` et sa structure est la suivante.

``` yaml
name        : Nom du paquet
version     : Numéro de version
release     : Numéro de release actuelle (incrémentation +1 à chaque MAJ)
source      : Source pour télécharger l'archive
license     : Licence du logiciel
component   : Groupe du paquet, liste dispo via la commande *eopkg lc*
summary     : Résumé du logiciel
description : Plus ample description
builddeps  :  Dépendances pour construire, faire 
fonctionner le logiciel si nécessaire.
  - pkgconfig(lib)
  - paquet-devel
setup      :|  Lancer la configuration si nécessaire
  %configure
build      :|  Lancer la construction du paquet
  %make
install    :|  Emplacement d'installation du paquet
  &make install
```

#### Builddeps

Certaines librairies et autres embarquent des noms `pkgconfig`, ce sont ces noms dont nous aurons besoin lors du build pour créer le paquet :
  - par exemple `gtk+-3.0` est contenu dans `libgtk-3-devel`, quand le build commence il recherche le nom *standard* de gtk+-3.0 et ça lui dit comment build against gtk3. (ou regarder pour trouver les librairies et les fichiers header, et quelles librairies utiliser)
en gros l'avantage ici c'est que le build system retrouve tout seul comme un grand ce qu'il faut.

  - attention cependant tous les paquets ne contiennent pas des pkgconfigs, on peut le vérifier en tapant `sudo eopkg info libgtk-3-devel` par exemple et la réponse sera :  
  `Provides: pkgconfig(gtk+-3.0) pkgconfig(gdk-3.0)...`.
  - Pour les paquets ne disposant pas de pkgconfig, il suffit de lister le paquet devel. Si ce dernier n'est pas disponible, lister le paquet normal.

Les version de paquet avec le suffixe *-devel* contiennent les fichiers nécessaires pour construire un paquet comme les headers, des fichiers .so, .pc etc...

une fois le fichier *package.yml* correctement documenté 
il suffit de lancer la commande `sudo solbuild build` à la racine du dossier comprenant votre fichier `package.yaml` et le build commence.  
Lors du build, le package .eopkg est crée ainsi qu'un package nommé *pspec_x86_64.xml*. Ce dernier est un fichier résumant le contenu du package et les informations sur l'empaqueteur (**Vous** !!)

## Soumettre des paquets

Pour soumettre des paquets dans les dépôts Solus il faut passer par le [Phabricator](https://dev.getsol.us). Il est conseillé de faire une recherche pour s'assurer qu'une demande d'inclusion n'a pas déjà été faite (et peut-être rejeté), qu'une soumission n'a pas été faite (pour une mise à jour ou après l'acceptation de l'inclusion.) Ensuite il suffit de suivre quelques règles :

### Paquet non présent dans les dépôts

Il faudra tout d'abord passer par une demande d'inclusion en précisant le nom du paquet, le site web (ou lien git), la raison de la demande (et si une alternative est déjà présente dans les dépôts qu'apporte de plus votre demande), si vous pensez que le logiciel sera utile/utilisé par beaucoup de monde (ou préciser la fourchette d'utilisateurs) ainsi que le lien vers l'archive du code source (Solus n’accepte pas les liens vers l'archive master d'un dépôt git, seulement les tags/releases sont autorisés).

### Paquet déjà présent / intégration acceptée

Il s'agit donc d'une inclusion ou d'une mise à jour. Les procédures sont similaires et commencent comme ci :

  - Une fois le paquet construit et testé il faut nettoyer le dossier avec `make clean` pour supprimer les paquets `.eopkg`.
  - on enchaine ensuite les commandes `git add .` et `git commit` pour ajouter les changements au dépôt.
 
L'envoie sur *Phabricator* se fait via `arcanist`. Il faut donc installer le paquet et le configurer.

``` bash
sudo eopkg it arcanist
arc set-config default https://dev.getsol.us
arc install-certificate
```

La troisième commande vous enverra sur le site pour vous connecter avec votre compte et vous attribuer un jeton personnel pour autoriser `arc`à échanger avec *Phabricator*.

Ensuite utiliser la commande `arc diff`, le logiciel se débrouille tout seul et vous propose de remplir un schéma pour ensuite le poster. Remplissez-le et voilà c'est terminé.

Pour aller plus loin je vous recommande d'aller lire la [doc officielle](https://getsol.us/articles/packaging/) de Solus. 
