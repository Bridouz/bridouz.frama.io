+++
title = "Blogus Livrus - Ray's Day 2018"
date = 2018-08-22T18:10:00+08:00
[taxonomies]
tags = ["Culture", "Livres"]
+++

Je l'avais loupé l'année dernière alors hors de question de le louper cette année. Aujourd'hui 22 août nous [fêtons l'anniversaire](https://raysday.net) de la mort de **Ray Bradbury**, amoureux des mots et écrivain témoignant d'une rare poésie dans sa prose.

Voici donc un petit concentré de mon histoire livresque.

![Ray was one of them](/img/livres/alienbradbury.jpg)

Je pense avoir de la chance en ayant des parents avides de lecture qui, lors de mon enfance, m'ont toujours encouragé à lire. Tout petit en passant par les célèbres *Astrapi*, *L'école des loisirs*, en m'emmenant à la bibliothèque. La chance était là également quand, le soir emmitouflé dans ma couette, j'écoutais d'une oreille somnolante mes parents me lire des histoires.

*J'aime Lire* fut aussi un passage important, l'attente du petit feuillet rouge pour lire et rigoler avec les aventures de *Tom-Tom et Nana*. D'autres noms comme *Rahan*, *Asterix* ou encore *Tintin* m'ont également accompagné. **Gotlib** fut sans doute le premier atome crochu littéraire partagé avec mon père. Un phrasé réfléchi, poétique et drôle capable de toucher toutes les générations, amenant des degrés de lecture inédits au fil des années. (Ce propos vaut pour *Gotlib* pas pour ce fou de père. Quoique...)

Adolescent j'ai doucement délaissé la lecture en faveur d'un écran rétro-éclairé et son clavier (les lettres n'étaient pas loin). De l'entrée du collège jusqu'à la terminale je n'ai plus lu par plaisir, simplement par obligation scolaire.  
Seul la lecture de *La ligne verte* me marque profondément, car elle s'associe pour la première fois à l'écoute d'un album. (*Six Degrees of Inner Turbulence* de **Dream Theater** et plus précisément le morceau [The Glass Prison](https://duckduckgo.com/?q=pr%C3%A9cisement&t=qupzilla&ia=web))  
Un moment rare qui, des années après, me renvoie toujours des images, des mots, des sons. C'est marrant comme le mariage du mot et du son peut rester ancré en nous.

Dernière année au lycée et je reprends goût à la lecture via les journaux. Ceux d'informations régionales dans un premier temps, ceux d'analyse politico-satirique à la maison. La reprise de la lecture est là mais, je ne le sais pas encore. Un ami me fait découvrir les comics, devant le lycée avant la sonnerie du matin. Un *Deadpool* sur un écran 5 pouces avec Android Froyo (balbutiements smartphoniques). Puis je m'y intéresse un peu plus et dévore les Deadpool donc puis l'event *Civil War* de **Marvel**.  
C'est alors que je redécouvre un héros de mon enfance, **Batman**, qui, je l'ignorais, à une vie dessinée plus que riche outre-atlantique. Un monde infini s'ouvre alors à moi et je tombe rapidement dans la lecture compulsive de comics (super-héroïque et autres). Un pan de la culture américaine que je ne connaissais pas, me paraissant  sans autre intérêt que le divertissement *blockbuster*. Je découvre alors des pépites comme *Watchmen*, *V pour Vendetta* et des dessins bluffants comme dans *House of M*, *Batman Hush* ou encore *Daredevil Guardian Devil*.

![Daredevil Guardian Devil](/img/comics/daredevilguardiandevil.jpg)

Le lycée est fini et je prépare ma rentrée en faculté d'anglais *littérature et civilisatrion*. La HP Touchpad arrive sur le marché pour repartir quelques semaines plus tard, un bide total qui me permettra d'avoir une tablette compétitive pour moins de 100 euros.  
*(7 ans plus tard elle est toujours vivante, me sert à lire mes comics et tourne pour 10 heures de lecture sans soucis et ce sur la dernière version d'Android en date. Prends ça dans les dents l'obsolescence programmée)*

Arrivé à la fac je me vois obliger de lire les classiques anglo-saxons et j'y découvre au détour de quelques oeuvres un plaisir renaissant à vibrer au fil des mots. Je rencontre des gens me poussant à lire, découvre la science-fiction, la fantasy, les romans à l'histoire implacable à l'Américaine. J'y découvre également la plume poétique d'un Paul Auster C'est d'ailleurs à ce moment-là que je découvre monsieur **Bradbury** avec *Fahrenheit 451*. Je rencontre madame également qui n'y est certainement pas pour rien non-plus dans ce renouveau littéraire.

Aujourd'hui j'ai repris goût à la lecture. Des comics encore et toujours et des bouquins, beaucoup de bouquins.  
SF, fantasy, Stephen King, romans américains, super-héros.

Si j'avais quelques conseils de *grandes* lectures à ce jour je dirais :

  - *La Horde du Contrevent* d'**Alain Damasio** pour le travail du texte, le voyage, la verve brute, le questionnement de soi. Mon numéro un incontesté et incontestable, une claque puissante. Une relecture prochaine est prévue et un billet est en cours de rédaction depuis 2 ans. (Une publication pour le RayDay 2019 ?)
  - *La Tour Sombre* de **Stephen King** (bouffez-en c'est un dieu vivant de l'histoire parfaite, des idées toutes bêtes ou trop bêtes qui partent en sucette)
  - *Les Sentiers des Astres* de **Stephen Platteau** (fantasy mythologique d'une rare finesse, une fresque écologique teinté d'influences multiples.)
  - *Of Mice and Men* de **John Steinbeck** (pur réalisme, histoire humaine incroyable, caractérisation attachante au possible)

Papier ou numérique peut importe. Le plaisir de lire n'a pas d'égal, l'harmonie et la puissance formé par le langage peut parfois toucher au plus profond de l'être.

*PS: N'hésitez-pas à partager vos grands moments de lecture dans les commentaires.*
