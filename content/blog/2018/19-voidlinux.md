+++
title = "Void Linux, la légèreté véloce"
date = 2018-12-20T12:00:00+01:00
[taxonomies]
tags = ["Linux"]
+++

Dans le monde linux j'affectionne tout particulièrement deux courants. Le premier se veut vraiment orienté sur le confort d'utilisation pour une personne lambda avec une interface graphique propre, fonctionnelle, une distribution plus que stable qui permet à une personne sans connaissance d'utiliser son OS simplement et sans soucis. Le deuxième est destiné aux utilisateurs souhaitant jouer avec le système et permet de créer un environnement propre à chaque utilisateur. Tout ou presque est à paramétrer selon l'utilisation que l'on souhaite faire de son OS.

Ainsi j'utilise deux distributions : **Solus** dont je parle régulièrement et plus récemment **Void**.

J'en entends parler depuis quelques mois mais je ne pensais pas qu'elle avait autant de bouteille. Sa date de création remonte à 2008, par *Juan Romero Pardines* qui auparavant développait pour *NetBSD*. Au vue du nombres de logiciels disponibles dans les dépôts nul doute qu'elle a fait du chemin depuis, et qu'elle a l'air d'avoir un build system plus que robuste avec de l'optimisation et plusieurs architectures disponibles. **Void** vient également avec son outils d'empaquetage maison *xbps* et est une distribution *rolling-release*.


## L'installation

L'image de base pour l'installation fait ~350MO, et s'installe en 5 minutes montre en main. L'installateur, justement, est en mode texte et est d'une grande clarté, il fait le job sans sourcilier. Nul besoin de déjà se compliquer la vie à l'installation de l'OS comme peut le faire *Arch*. On peut vouloir se configurer un OS aux petits oignons sans pour autant se taper l'installation basique. (**OpenBSD** permet également d'installer sa base en un rien de temps.) Très bon point donc.

Au premier redémarrage je suis surpris par la vitesse de boot de l'OS. Je gagne facilement ~8 secondes par rapport à un OS utilisant systemd. Avec l'image de base je tombe sur la ligne de commande. Ensuite libre à chacun d'installer ce qu'il souhaite.

Des images avec *xfce*, *lxde*, *lxqt*, *enlightement*, *mate ou encore *cinnamon* sont disponibles et allant de 650 à 850MO. Personnellement j'ai testé l'image *xfce*, le look est classique, sans fioriture et on retrouve ce DE tant aimé sans la moindre fausse note. À noter que si l'on veut installer l'OS avec l'une de ces images il fait passer par la ligne de commande pour lancer `void-installer`.


## Runit

**Void** n'utilise pas *systemd* et lui préfère *runit* un système d'init plus léger qui fonctionne essentiellement à base de liens symbolique. Pas de possibilité de lancer les services par utilisateurs, ici on s'occupe des services de fond pour l'OS dans sa globalité.

Je trouve que ce système d'init est vraiment pas mal. Il fonctionne efficacement et simplement, les liens symboliques sont rapides à mettre en place.

    sudo ln -s /etc/sv/*** /var/services/

les arguments `start`, `status` et `stop` peuvent être utiliser pour gérer les différents services.

Pour le moment pas de gestion de service par utilisateur, cependant cela est tout de même possible après deux, trois bidouilles.


## Musl

La distribution propose deux choix pour libc , *glibc* et [*musl*](https://www.musl-libc.org/).

Je n'y connais pas grand chose mais de ce que je lis **Musl** est une alternative à **Glibc**
se voulant mieux écrite et plus sécurisée.
Firefox marche, feh, zathura, transmission également. Je n'ai rencontrer des soucis que lors de tests avec Qupzilla où le moteur de rendu QtWeKit plantait joyeusement. Le problème est identifié et des développeurs travaillent dessus.

En revanche Musl ne peut, pour le moment, gérer les différentes langues du système via *local* ce qui fait que l'on se retrouve avec un OS forcément en anglais. Ce n'est pas rédhibitoire mais bon à signaler.

## Bilan

Pour le moment je trouve tout ce dont j'ai besoin, même des outils plus *BSD* comme **stagit** sont de la partie. Niveau DE on retrouve les plus connus *Gnome*, *KDE* ou encore *Xfce*, du côté des wm il y a également du choix. Et niveau logiciels on retrouve tous les cadors autant niveau GUI que CLI.

**Xbps** fait le boulot pour installer, supprimer et rechercher. le processus est limpide et plus que rapide.

le système d'empaquetage semble plus que roder et simple à prendre en main. Des outils maisons sont mis à disposition pour simplifier le processus et les environnements de builds type *cmake*, *go build* ou encore *python3*. Il est possible de contribuer au dépôt via *Github*, J'espère que **Void** continuera de grandir et que son infrastructure migrera un jour vers autre chose que Github, mais pour le moment ça tourne vraiment bien et l'équipe est plus que réactive pour modérer les PRs.

Je suis agréablement surpris par cette distribution qui se révèle être une très bonne distribution pour du *sur-mesure*. Je tourne actuellement avec **bspwm** (config piquée [ici](https://github.com/addy-dclxvi/void-bspwm-dotfiles)) et beaucoup d'outils en ligne de commande (*zathura* pour le pdf, *feh* pour les images, *ranger* comme gestionnaire de fichier). Ça tourne plus que bien sur mon vieux pc portable sans pour autant trop solliciter le cpu et me déclencher le ventilo de la mort (marque déposée). Scanner et imprimantes fonctionnent sans soucis également.

**Runit** apporte un gestionnaire init plus que simple et léger, et la quantité de paquets disponibles dans le dépôt est un réel plus. Nul de besoin de passer par un *AUR* pouvant s'avérer source de problèmes.

Si l'aventure vous tente je recommande chaudement.


---
Liens:
  - [Void Linux](https://voidlinux.org/)
  - [Musl](https://www.musl-libc.org/)
  - [Solus](https://getsol.us/)
