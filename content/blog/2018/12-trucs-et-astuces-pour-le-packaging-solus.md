+++
title = "Trucs et astuces pour le packaging sous Solus"
date = 2018-11-07T16:00:00+01:00
[taxonomies]
tags = ["Solus", "Contribution"]
+++

Suite à mon [précédent billet](@/blog/2018/11-construire-des-paquets-sous-solus.md) sur le packaging sous **Solus** je vous propose aujourd'hui un court billet avec diverses astuces pour faciliter la vie de l'empaqueteur. C'est parti

Quand on débute un empaquetage pour un logiciel non-présent dans les dépôts il faut dans un premier temps créer un fichier `Makefile` et initialiser un dépôt git qui permettra de suivre les changements d'empaquetage.  
Pour cela j'utilise un `alias` dans `.bashrc`.

```
alias mkf='echo "include ../Makefile.common" > Makefile && git init .'
```

Lors de ce premier empaquetage il est également essentiel de créer un fichier `package.yml`. Ce dernier servira à construire le paquet en donnant les informations nécessaire aux outils de construction. Un script est disponible pour *automatiser* la première création de ce fichier. Je colle donc un `alias` dans le fichier `.bashrc`. Il suffit alors d'appeler `yauto.py` avec en argument le lien vers l'archive contenant les sources du logiciel.

```
alias yauto="/home/justin/Solus/Solbuild/common/Scripts/yauto.py"
```

Si je lance `yauto https://www.nano-editor.org/dist/v3/nano-3.1.tar.xz`

```
name       : nano
version    : 3.1
release    : 1
source     :
    - https://www.nano-editor.org/dist/v3/nano-3.1.tar.xz : 14c02ca40a5bc61c580ce2f9cb7f9fc72d5ccc9da17ad044f78f6fb3fdb7719e
license    : GPL-2.0-or-later # CHECK ME
component  : PLEASE FILL ME IN
summary    : PLEASE FILL ME IN
description: |
    PLEASE FILL ME IN
builddeps  :
setup      : |
    %configure
build      : |
    %make
install    : |
    %make_install
```

Après avoir vu des astuces pour un premier empaquetage d'un logiciel, voyons celles pour une mise à jour.

Il faut dans un premier temps récupérer le dépôt git chez **Solus** contenant le fichier `package.yml` et autres fichiers. J'ajoute un `alias` dans le fichier `.gitconfig` pour simplifier le clonage.

```
[url "https://dev.getsol.us/source/"]
    insteadOf = solus:
```

Ainsi pour cloner le dépôt `nano` il suffira de taper `git clone solus://nano`.

Ensuite lors d'une montée en version d'un logiciel il faut modifier plusieurs lignes du fichier `package.yml`.

```
version    : 3.1
release    : 1
source     :
    - https://www.nano-editor.org/dist/v3/nano-3.1.tar.xz : 14c02ca40a5bc61c580ce2f9cb7f9fc72d5ccc9da17ad044f78f6fb3fdb7719e
```

La ligne la plus embêtante est sans doute celle `source` car la structure de l'url peut parfois changer et surtout il faudra recalculer la somme `sha256`. Un script est mis à disposition avec `ypkg` pour faciliter cela, son nom : `yupdate.py`. Un `alias` dans `.bashrc` plus tard et c'est parti.


```
alias pkgup="/usr/share/ypkg/yupdate.py"
```

le script prend deux arguments dans un ordre précis. Le premier est le numéro de la nouvelle version et le deuxième est l'url de cette dernière.

```
yauto 3.1 https://www.nano-editor.org/dist/v3/nano-3.1.tar.xz
```

Le script fait le job, modifie les lignes nécessaires dans le fichier `package.yml` et il ne reste plus qu'à lancer la construction.

Voilà pour les quelques astuces que j'utilise. Si vous en utilisez d'autres n'hésitez pas à les partager dans les commentaires.
