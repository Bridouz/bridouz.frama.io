+++
title = "Plasma et Solus"
date = 2018-07-05T10:00:00+02:00
[taxonomies]
tags = ["Linux", "KDE", "Solus"]
+++

**Solus** est la distribution que j'utilise au quotidien depuis maintenant plus de deux ans, et que madame a adopté depuis presque un an. C'est une distribution *relativement* petite mais qui a le mérite d'apporter une attention toute particulière au confort de l'utilisateur. Là où il y a quelques années j'aimais l'instabilité je me trouve bien heureux d'avoir des passionnés offrant un environnement stable sans oublier d'avoir des logiciels à la pointe.

Bref, **Solus** donc, et du coup **Budgie** et des logiciels *GNOME/Gtk*. Pour le DE j'aime beaucoup, simple et élégant, sans trop de configuration ce qui permet de se concentrer sur l'usage et non la forme. Cependant les logiciels GNOME me paraissent assez lourds pour mon vieux coucou et surtout, j'y vois une uniformité manquante, un manque de je-ne-sais-quoi.  
Ce qui gène donc un peu et, les vacances et le temps étant enfin réunis, pourquoi ne pas en profiter pour aller voir du côté de *KDE/Qt*, j'en entends du bien depuis un moment et j'ai été un fervent utilisateur de sa quatrième mouture il y a quelques année.

Surtout que *Plasma* est arrivé depuis quelque temps dans les dépots de Solus. Tout n'est pas encore là mais les packagers s'affèrent à combler les trous.  
Un `sudo eopkg it -c desktop.kde.core` et me voilà avec un environnement **Plasma** minimal, sans fioritures.


Globalement je trouve que le DE a fait de gros progrès depuis ~2 ans. À l'époque le gestionnaire de fenêtre *Kwin* plantait souvent sur ma bécane, les logiciels n'étaient pas encore tous portés vers *Qt5*, ce qui entraînait des incohérences au niveau de l’expérience utilisateur, ainsi que des plantages.  
*Plasma planta edition*, c'était son surnom.

Après quelques jours d'utilisation je dois bien avouer que les développeurs ont fait un boulot de dingue et que le DE est devenu un vrai plaisir à utiliser.  
Parmi les points positifs de **KDE Plasma 5(.13)** je noterais:

  - **Kate** : l'éditeur de texte qui peut être grandement customisé. Partir d'un éditeur rapide à un *presque* IDE avec accès rapides aux projets *git*, terminal à disposition, outils pour la complétion syntaxique, possibilité de scinder l'écran verticalement ou horizontalement, colorisation syntaxique et bien d'autres.
  - **L'utilisation RAM** : 420MO sans rien. Ça laisse de la place pour les autres logiciels. Et c'est vraiment un point que *KDE* a grandement travaillé. il y a quelques années j'étais plus proche des 800MO. Bien joué. De plus la libération de **RAM** une fois les fenêtres/logiciels fermés est rapide. Sympa, sympa.
  - **Falkon** : Un navigateur web léger et fonctionnel, ayant intégré récemment le giron de KDE pour l’hébergement git. J'utilisais *Firefox* auparavant, mais il faut bien dire qu'il commençait vraiment à devenir lourd. Là l'occupation de RAM en est divisé par ~2. Certes il n'y a pas pléthore d’add-ons mais l'essentiel *bloqueur de pub* est présent. Et c'est tout ce que je demande.
  - **Utilisation CPU** : j'ai également été surpris par la réactivité des logiciels et  du bureau en général. L'utilisation CPU n'est certe pas minime mais elle redescend vraiment rapidement une fois le logiciel fermé.
  - **Kwin** : gestionnaire de fenêtre aussi configurable que le reste, avec un tas d'extensions. Et dans le mot il y en a même une pour le tiling [Quarter Timing](https://github.com/Jazqa/kwin-quarter-tiling). Ça marche du tonnerre ! *Je vous conseille cependant d'installer manuellement en suivant les indications sur Github. La version présente sur le KDE Store est daté et un poil bugé lors de mes tests.*
  
  **Akonadi** prend trop de RAM avec sa base Mysql ? (chez mois j'étais arrivé à ~300Mo rien que pour ce dernier)  
  Pas de soucis, il suffit d'éditer le fichier `~/.config/akonadi/akonadiserverrc` et de supprimer la partie *Mysql* et de changer le `Driver` de `QMYSQL` à `QSQLITE3`. Avec ça j'arrive à 630Mo en arrivant sur ma session. Bureau + Nextcloud + Daemon akonadi + mpd. Plutôt correct.

## Modularité

> Simple by Default, Powerful When Needed

**KDE** s'exprime beaucoup sur ce sujet et effectivement les développeurs ont réussis à créer quelque-chose de cohérent, uniforme et modulable à souhait. À l'installation, un environnement *Plasma* peut directement être utilisable, les logiciels peuvent être facilement installés et on se retrouve rapidement avec un bureau fonctionnel et complet.

Les goûts aux niveaux de l'apparence varient selon les utilisateurs alors je ne traiterais pas ce sujet. Notons tout de même la grande variété de thèmes/icônes disponibles.

Ensuite, libre à vous d'explorer les paramètres du bureau ou des logiciels pour faire vos petits arrangements au fil du temps. Personnellement c'est ce que j'aime chez **KDE**. Les outils sont déjà agréables à utiliser avec des paramètres *sortie d'usine*, mais il est possible de tout parametrer comme on le souhaite.  
Le nombre de raccourcis clavier est absolument immense. Les logiciels **K** ont quasi tous des fonctionnalités supplémentaires, installables avec des paquets *-addons* et surtout ils sont complets.


## Cohérence

Le design se rapproche plus de ce que l'on connaît tous, notamment avec *Windows*. C'est classique et efficace. Là où je pouvais reprocher à Gnome de faire des choix un peu étranges. (cf le menu logé dans la barre, qui soit-dit-en passant va être réintégré à la fenêtre des logiciels pour Gnome 3.32)  
Les deux DE n'ont pas la même philosophie, inutile donc de les comparer et d'alimenter une guerre numérique qui n'a pas lieu d'être.

De plus l’émergence du framework [*Kirigami*](https://www.kde.org/products/kirigami/) permet d'avoir un design plus que sympa et surtout, pouvant être cohérent avec une utilisation sur smartphone. En vue d'un éventuel **Plasma Mobile** C'est plutôt pas mal.  
En tout cas je m'y retrouve complètement avec *Plasma 5.13*. Un bon DE : complet, pratique, intuitif, modulable et ne bouffant pas non plus toute la RAM. Un vrais coup de cœur.

Et que dire du travail effectué chez *Solus* ? Encore une fois l'intégration est propre, sans bavure (où alors je ne les vois pas.) La fonction de mise à jour *delta* est un réel plus vu la rapidité de sorties des versions de **Plasma**. Quelques logiciels manquent encore à l'appel cependant alors, je m'en vais ressortir mes outils de construction et vais dès maintenant essayer d'empaqueter deux trois logiciels.


![Plasma 5.13 en action](/img/solus/plasmatiling.png)
