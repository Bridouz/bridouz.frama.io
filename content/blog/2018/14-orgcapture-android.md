+++
title = "Org-Capture sous Android"
date = 2018-11-16T00:00:00+01:00
[taxonomies]
tags = ["Emacs", "Android", "Termux"]
+++

Toujours envoûté par _emacs_, l’interaction devient de plus en plus fluide, mon cerveau commence à sortir les raccourcis claviers intuitivement. Sous Linux c'est déjà top et depuis peu je l'utilise également sous Android sur mon téléphone et ma tablette. Un éditeur plus que puissant dans la poche, et puis _org-capture_ et ça, c'est chouette.

Pour ceux qui ne connaissent pas, [Termux](https://termux.com/) est une application _Android_ officiant comme un émulateur de terminal avec une belle collection de paquets **Linux**. On peut y faire beaucoup de choses : éditer des fichiers avec `nano`, musique avec `mpd`, IRC avec `weechat`, du `ssh`, du `python`, gérer ses dépôts avec `git`  et bien évidement `emacs`.


## Termux-url-opener

En premier lieu il faut créer un fichier `~/bin/termux-url-opener` qui sera utilisé par _termux_ pour ouvrir les liens que les autres applications lui envoient. Pour ma part j'ai piqué un exemple sur le net que j'ai modifié pour qu'il puisse me proposer _org-capture_.

```bash
#!/data/data/com.termux/files/usr/bin/bash

link=$1

echo "What should I do with $link ?"
echo "c) Org-capture"
echo "l) Capture links"
echo "x) nothing"

read CHOICE
case $CHOICE in
  c)
    emacsclient org-protocol://capture?
    ;;
  l)
    emacsclient "org-protocol:/capture?template=p&url=$link&title=$link"
    ;;
  x)
    echo "Bye"
    ;;
esac
```

On remarque donc qu'en appuyant sur `l` L’URL sera transmise à emacs, me permettant de sauvegarder le lien rapidement. L'option `c` me permet juste d'aller sur le menu de sélection de templates `org-capture` depuis n'importe quelle application en partageant vers `termux`.


## termux-boot

Ensuite vient le cas du chargement d'emacs qui peut s'avérer assez long (quelques secondes). pour éviter cela il est possible de lancer `emacs --daemon` pour ensuite se connecter rapidement avec `emacsclient -c`.

Il suffit de créer un fichier d'installer [Termux:Boot](https://wiki.termux.com/wiki/Termux:Boot), de créer un fichier dans `termux/boot/` et y placer `emacs --daemon`. Ainsi au lancement du smartphone emacs se lancera en mode daemon. Notez cependant que l'OS peut entrer en veille profonde avant de lancer le script et donc tout faire capoter. Il suffit d'ajouter `termux-wake-lock` au script pour empêcher la mise en veille au démarrage.

Cependant tant que `termux-wake-lock` est activé le téléphone ne rentrera pas en sommeil profond et l'impact sur la durée de vie de la batterie se fera sentie. Il suffit alors d’appuyer sur `release wakelock` dans la notification termux. J'ai essayé de caser `termux-wake-unlock` dans le script, sans succès.

Voilà maintenant org-capture est disponible sous Android et la sauvegarde de liens bien organisée devient un jeu d'enfant.
