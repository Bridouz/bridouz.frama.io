+++
title = "Améliorer Gedit"
date = 2018-06-11T18:15:00+02:00
[taxonomies]
tags = ["Linux", "Gnome", "Tips"]
+++

Outre **Firefox** et **mpd** l'outil que j'utilise le plus est sans doute **Gedit**, l'éditeur de texte par défaut pour l’environnement *GNOME* (*Budgie* dans mon cas). Écrire des notes, des billets pour le blog, éditer des fichiers de configuration, etc. Alors quand on utilise un outil assez fréquemment il est souhaitable qu'il soit complet (tout en étant simple à utiliser). **Gedit** est vraiment très facile à prendre en main et, de facto, propose le minimum vital pour écrire, mais il est possible de l'améliorer un peu pour qu'il devienne beaucoup plus puissant.

Pour cela, un seul paquet supplémentaire est nécessaire : `gedit-plugins`.  
Une fois installé il n'y a plus qu'à se rendre dans l'onglet *Greffons* du panneau *Préférences* et d'activer les plugins qui vous intéressent.

Pour ma part j'apprécie tout particulièrement:

  - Complétion de parenthèses
  - Espaces intelligents (Forget you're not using tabulations)
  - Fragments de code (par défaut avec Gedit)
  - Surlignage des lignes ayant changé depuis le dernier commit
  - Indicateur d'espaces
  - Panneau d'explorateur de fichiers
  - Recherche dans tous les fichiers d'un dossier

![Améliorer Gedit](/img/solus/geditplugins.png)

Avec ça l'édition est beaucoup plus agréable et permet de garder un outil simple et intuitif et de ne pas migrer vers les lourdeurs que son *Atom* et *VS Studio*, surtout quand on est pas un codeur de métier.

PS: Billet écrit avec `vim` :)
