+++
title = "Alzheimer, prendre soin sans oublier"
date = 2018-11-20T00:00:00+01:00
[taxonomies]
tags = ["aidesoignant", "santé"]
+++

_Ce billet ne parlera que des EHPAD publics et de mes expériences. Le privé est une aberration sans nom, une honte pour la dignité des résidents et un contre-sens total sur la vocation de soignant._

Alzheimer, un nom dont on parle de plus en plus souvent, car la maladie se déclare de plus en plus et de plus en plus tôt. À ce jour on comptabilise ~900 000 malades de la mémoire. L'impact de la maladie sur ces personnes varie mais, il arrive parfois un moment où la possibilité de rester à la maison n'est plus possible. Soit les malades ne peuvent plus du tout assurer une vie quotidienne en toute sécurité soit, et c'est un critère assez répandu, les aidants sont à bout et ne peuvent plus s'occuper de leur parent.

Cet épuisement est **normal** car, la maladie d'Alzheimer est cruelle. Elle renvoie le plus souvent un inversement des statuts familiaux. Là où l'oublie apparaît, le fils peut devenir le père pour un père qui ne se rappelle plus avoir d'enfant. Cette maladie perturbe tout une structure établie, redistribue les cartes. De plus, bien qu'il y ai des points communs, des ressemblances entre les malades de la mémoire on remarque tout de même que la maladie s'exprime différemment chez chaque individu.


## Déni

Une des choses compliquées avec cette maladie de la mémoire est qu'il faut souvent faire face au **déni**. Celui de la personne atteinte en début de maladie, masquant les troubles pour ne pas faire apparaître au grand jour la maladie. Celui des familles par la suite, refusant d'accepter ce que leur parent est devenu, ou plutôt ce que la maladie a modifié en lui. Et le plus souvent avec un malade cachant le réel avancement de la maladie, lorsque la famille remarque qu'il y a quelque-chose qui cloche c'est le plus souvent trop tard. La maladie est déjà présente depuis des années et son statut neurodégénératif fait qu'elle ne pourra être stoppée. Peut-être retardée si prise à temps mais ce n'est qu'une situation optimale.

Lorsque la maladie dépasse les aidants le parent est ainsi pris en soin par une institution. Il est important de préciser que le malade de la mémoire peut, selon l'impact qu'a sa maladie sur lui et les autres, rentrer en EHPAD dans une unité _classique_. Où, lorsque la maladie présente un danger pour la personne ou l'entourage, les **UHR** (unité d’hébergement renforcée) peuvent être une solution d'accueil en phase aiguë.<br />
Dans ces unités spécifiques la personne y trouvera du personnel formé, des structures adaptées pour prendre en soin et stimuler ses fonctions résiduelles.


## Des petits rien pour stimuler

Faire de grands projets pour un malade de la mémoire est le plus souvent quelque-chose d'irrationnel. Là où la dégénérescence peut être atténuée se trouve dans la capacité à entretenir la mémoire résiduelle (odeur, mécanique etc) et à stimuler la mémoire procédurale pour recréer des automatismes.<br />
Installer le nécessaire de toilette toujours de la même façon, inscrire un ordre dans le déroulement du soin permet, selon l'avancement de la maladie, de créer un automatisme, des mouvements répétés, sans réfléchir.

Un malade de la mémoire n'est pas forcément incontinent par exemple, seulement il ne sait, ne comprend plus à quoi servent les toilettes ou bien exprimer l'envie d'éliminer. La mémoire procédurale permet d'essayer de faire entrer ce geste comme une habitude, un geste automatique. _(Ce dernier paraissant logique pour un cerveau non atteint ne l'est pas forcément pour un cerveau endommagé)_


## Une chute inéluctable

Et la partie la plus dur sans doute est le côté neurodégénératif de la maladie qui, pour le moment, ne peut qu'être ralentit et non enrayé.

Il y a des moments durs à voir d'un point de vue humain. Le regard _vide_ et fixe d'une personne est quelque-chose qui me rend instantanément triste. L'incompréhension dans l'échange est une source d'angoisse profonde pour le malade de la mémoire, pouvant parfois exprimer les choses dans un cadre différent que celui qui entend. Une douleur peut se manifester par une marche incessante, une constipation par de l'agitation et de la confusion.


## Ne pas oublier la personne derrière

Il y a ceux qui parcourent 30km par jours, infatigables marcheurs en quête d'une chose qu'eux seul peuvent expliquer sans réussir à l'exprimer. Ceux qui oublient sans cesse, n'enregistre plus et angoisse, angoisse. Ceux qui ne reconnaissent plus leur entourage, qui ne reconnaisse plus cette vieille personne dans le miroir alors que leur esprit leur renvoie un âge bien inférieur. Ceux chez qui la maladie n'a pas tout emporté et qui, dans de brefs moments, se rendent compte de leur maladie et de ses répercussions. Là aussi c'est une chose dur à voir.<br />
Cet homme qui ne devrait pas être à la retraite et qui, pourtant, est bien devant toi quand tu arrives dans l'unité. Cet homme qui souffre, qui se renferme pour ne pas faire souffrir ses proches. Cet homme qui, tu le sais, ne remontra pas la pente et qui sans accepter ton aide verra sa maladie progresser bien plus vite que tu ne peux l'imaginer.

Et puis il y a ces moments où toi, soignant, tu te poses et observes. Tu regardes une quinzaine de personnes dans une même pièce, marcher seuls, sans attaches. Tu regardes cette multitude de vies, d'histoires,de liens coupés avec le monde se mouvoir devant toi, sans avoir ce réflexe de discuter, de créer du lien. Alors tu t'assois avec eux, tu sers un café pour remémorer les bons souvenirs, tu parles de la pluie, du beau temps, tu stimules ce reste de mémoire qui leur ai inaccessible sans aide extérieur.

Tu entends la personne délirer et parler d'une époque révolue, tu ne casse pas ce délire _innoffencif_ non, tu essaies plutôt de lancer des accroches pour ramener la personne à revenir doucement à la réalité en stimulant sa mémoire. Tu ne t'énerves pas quand une personne malade te repousse alors que tu essaies de palier à son incontinence non, car tu sais au fond de toi que la personne ne te repousse pas **toi** mais plutôt ce geste **intrusif** qu'elle ne **comprend plus**.

Tu essaies de te renseigner sur cette vie passée, partiellement oubliée pour en tirer des informations qui te seront utile dans la prise en soin. Tu te rends le plus disponible, sans pression ni tension car tu le sais, les malades de la mémoire sont de vrais éponges et, si tu es énervé, stressé, alors eux le ressentiront et réagiront à ces émotions.

Prendre soin des malades de la mémoire, dans un exercice professionnel, est une des choses les plus intéressantes qu'il m’aie été donné de faire dans ce métier. Et si déjà vous partez du fait que la maladie, au vue des recherches actuelles, ne peut être guéri votre rôle est très simple :<br />
_Restaurer ou maintenir l'autonomie et assurer que la personne vive décemment, jusqu'à la fin inéluctable en prenoin soin que la personne ne souffre pas par votre faute._
