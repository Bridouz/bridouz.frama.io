+++
title = "Dépôts git en toute simplicité sur un Raspberry"
date = 2018-07-06T10:00:00+02:00
[taxonomies]
tags = ["Linux", "Raspberry", "Tips"]
+++

Git*hub/lab* sont des mots compte triple en ce moment sur le web. Fort heureusement **git** n'est pas simplement un outil pour ces forges. Certes elles l'utilisent et lui servent d'étendards, mais son utilisation ne s'arrète pas là.

*Je ne reviendrais pas sur l'actualité, libre à cachun de penser ce qu'il veut.*

J'utilise **git** pour plusieurs choses :

  - Gérer mon blog (utilisant la génération automatique avec Framagit).
  - Sauvegarder mes *dotfiles* (fichiers de configuration), au cas où mon ordi plante.
  - Utilisation avec [Password-Store](https://www.passwordstore.org/)
  - empaqueter des logiciels pour [Solus](https://getsol.us)

J'utilisais jusque-là *Gitea* comme interface. Un service écrit en *Go*, très léger et tournant vraiment bien su un *Raspberry*. Et comme souvent je me suis rendu compte que je n'exploitais pas toutes ses fonctionnalités alors autant utiliser simplement **git** pour créer des dépôts distants servant de miroirs/backups. Je n'ai nul besoin d'interface web, ces dépôts servent juste de sauvegardes pour mes différents usages.

Pour cela :

```bash
# Créer un utilisateur dédié pour git
sudo adduser --gecos "" git

# Créer le dépôt bare
git init --bare /home/git/repo

# Si créé avec un autre utilisateur
sudo chown git:git /home/git/repo
```

Si on souhaite sécuriser la connexion en autorisant seulement les accès via clés ssh préalablement autorisées :

```bash
# Copier la clé ssh du client
ssh-copy-id git@server

# Éditer le ficher /etc/ssh/sshd_config
PubkeyAuthentication yes
AuthorizedKeysFile %h/.ssh/authorized_keys
PasswordAuthentification no
PermitEmptyPasswords no
```

> Terminé

Et voilà, maintenant il n'y a plus qu'à rajouter le remote
`git@votreserveur:/home/user/repo` à vos projets et le tour est joué.
